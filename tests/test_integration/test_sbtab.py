"""A test module for SBtab I/O."""

# The MIT License (MIT)
#
# Copyright (c) 2013,2020-2023 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest
from path import Path

from equilibrator_api.model import open_sbtabdoc


@pytest.fixture(scope="module")
def test_dir() -> Path:
    """Get the test directory."""
    return Path(__file__).abspath().parent


def test_from_network_sbtab(test_dir):
    """Test reading SBtab documents."""
    sbtab_filename = str(test_dir / "example_fermentation.tsv")

    sbtabdoc = open_sbtabdoc(sbtab_filename)

    open_sbtabdoc(sbtabdoc)

    with pytest.raises(ValueError):
        open_sbtabdoc(10)
