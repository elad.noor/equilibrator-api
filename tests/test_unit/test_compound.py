"""unit test for PhaseReaction and ComponentContribution."""

# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import numpy as np
import pytest

from equilibrator_api import Q_
from equilibrator_api.phased_compound import (
    AQUEOUS_PHASE_NAME,
    LIQUID_PHASE_NAME,
    PHASE_INFO_DICT,
    SOLID_PHASE_NAME,
    PhasedCompound,
)


@pytest.mark.parametrize(
    "inchi, expected_phase",
    [
        ("InChI=1S/N2/c1-2", AQUEOUS_PHASE_NAME),  # N2
        ("InChI=1S/S", SOLID_PHASE_NAME),  # Sulfur
        ("InChI=1S/H2O/h1H2", LIQUID_PHASE_NAME),  # H2O
    ],
)
def test_default_phase(inchi, expected_phase, comp_contribution):
    """Test some basic phase functions for compounds."""
    cpd = comp_contribution.get_compound_by_inchi(inchi)
    phased_cpd = PhasedCompound(cpd)
    assert phased_cpd.phase == expected_phase
    assert phased_cpd.phase_shorthand == PHASE_INFO_DICT[expected_phase].shorthand
    assert phased_cpd.get_stored_standard_dgf() in {Q_(0, "kJ/mol"), None}

    if PHASE_INFO_DICT[expected_phase].standard_abundance is None:
        assert phased_cpd.ln_abundance == 0
        with pytest.raises(ValueError):
            phased_cpd.abundance = Q_("1 M")
        assert phased_cpd.is_physiological
    else:
        assert not phased_cpd.is_physiological
        phased_cpd.abundance = Q_("1 mM")
        assert phased_cpd.ln_abundance == np.log(1e-3)
        assert phased_cpd.is_physiological

    assert phased_cpd.smiles is not None
