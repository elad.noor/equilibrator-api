Installation
============

Step 1
******
Install eQuilibrator using pip (ideally, inside a virtual environment):

.. code-block:: shell

    pip install equilibrator-api

If you are using Windows OS, we recommend using `conda` instead of `pip`:

.. code-block:: shell

    conda install -c conda-forge equilibrator-api


Step 2 (optional)
*****************
Run this command to initialize eQuilibrator:

.. code-block:: shell

    python -c "from equilibrator_api import ComponentContribution; cc = ComponentContribution()"


Note, that this can take minutes or even up to an hour, since about 1.3 GBytes of data need to be downloaded from a remote website (`Zenodo <https://zenodo.org/>`_). If this command fails, try improving the speed of your connection (e.g. disabling your VPN, or using a LAN cable to connect to your router) and running it again.

Note that you don't have to run this command before using eQuilibrator. It will simply download the database on the first time you try using it (e.g. inside the Jupyter notebook). In any case, after downloading the database the data will be locally cached and loading takes only a few seconds from then onwards.


Step 3 (optional)
*****************
Now, you are good to go. In case you want to see an example of how to use eQuilibrator-API in the form of a Jupyter notebook, run the following commands:

.. code-block:: python

    pip install jupyter
    curl https://gitlab.com/equilibrator/equilibrator-api/-/raw/develop/scripts/equilibrator_cmd.ipynb > equilibrator_cmd.ipynb
    jupyter notebook

and select the notebook called `equilibrator_cmd.ipynb` and follow the examples in it.


Dependencies
************

- python >= 3.8
- path
- numpy
- scipy
- pandas
- pyparsing
- tqdm
- appdirs
- diskcache
- httpx
- tenacity
- python-slugify
- periodictable
- sqlalchemy
- Levenshtein
- pint
- uncertainties
- cobra (optional)
- sbtab (optional)
